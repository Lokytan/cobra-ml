# Cobra Markdown Language (CobraML)

CobraML is an enhancement of plain markdown by extending it with a simple variable system.
It was initiated by DevilsJin in 2015 and is GPL v2.0 licensed.

## What is CobraML ?

CobraML is an extension to any markdown language (it isn't specifically bound to one syntax) which builds on-top of
the interpreters.

It provides the ability to declare variables and print them to screen.

## Requirements

The CobraML interpreter (`cobraml.py`) is completely written in **python 3** to support a vary of OS.

So the requirements for CobraML are...

* Python 3

## Installation

The installation of CobraML is very simple.

### Linux / Mac

On Linux and Mac machines we simply need to copy the `cobraml.py` to `/usr/local/bin/cobraml`.

```bash
sudo cp /path/to/cobra-markdown/src/cobraml.py /usr/local/bin/cobraml
```

> <font color=darkorange>ATTENTION: Make sure cobraml.py has execution rights (755)!</font>

### Windows

On Windows machines we need to add the `/cobra-markdown/src` directory to the user environment variable `PATH`.

```bash
set PATH=%PATH%;C:\ProgramFiles\cobra-markdown\src;
```

## Usage (CLI)

To simply convert documents on-demand from CLI we use the `cobraml` script (if not installed go to 'Installation').

### Arguments

| Argument | Keys | Default | Description |
|----------|------|---------|-------------|
| input | -/- | -/- | Input of Cobra Markdown |
| output | `-o, --output` | `False` | Output of plain Markdown |
| text | `-t, --text-input` | `False` | Flag for text-input |

#### Examples

```bash
# parse from cli to cli
cobraml -t '@var="# Hello";@var;'
# parse from cli to file
cobraml -t '@var="# Hello";@var;' -o output.mdc
# parse from file to cli
cobraml file.mdc
# parse from file to file
cobraml file.mdc -o output.mdc
```