#!/usr/bin/python
import os.path as path
import re as regex
import argparse


class Pattern:
    declaration = r'@([a-zA-Z]+[a-zA-Z0-9_-]+)="([^@]+)";\n*'
    usage = r'@([a-zA-Z]+[a-zA-Z0-9_-]+);'


class IO:
    file = 0
    cli = 1


# Cobra class which handles convertation of Cobra Markdown to plain Markdown.
class Cobra:
    # input file object
    input = None
    # output file object
    output = None

    # declared variables
    variables = None

    ##
    # Constructor
    #   @param input Path of cobra file
    #   @param output Path to output file
    def __init__(self):
        self.variables = {}

    ##
    # Parse cobra markdown
    #   @return Plain markdown
    def parse(self, input=None, output=None, inputType=IO.file, outputType=None ):
        cobra = None
        markdown = None

        if inputType == IO.file:
            cobra = open(input,'r').read()
        elif inputType == IO.cli:
            cobra = input

        # replace and save variable definition
        markdown = regex.compile(Pattern.declaration).sub(self.regex_declaration, cobra)
        # replace variable tokens with value
        markdown = regex.compile(Pattern.usage).sub(self.regex_use, markdown)

        # print result or save it to file
        if outputType == IO.cli:
            print markdown
        elif outputType == IO.file:
            out = open(output,'w+')
            out.write(markdown)

        # return markdown
        return markdown

    ##
    # Regex callback for declaration matches
    #   @param declarationMatch Information about match
    #   @return
    def regex_declaration(self, declarationMatch):
        self.variables[declarationMatch.group(1)] = declarationMatch.group(2)
        return ""

    ##
    # Regex callback for use matches
    #   @param useMatch Information about match
    #   @return related value of match
    def regex_use(self, useMatch):
        return self.variables[useMatch.group(1)]

# Generate CLI argument parser and add arguments
cli_parser = argparse.ArgumentParser(description="Converts a cobra markdown to plain markdown.")
cli_parser.add_argument('input')
cli_parser.add_argument('-o', '--output', action="store", dest="output", default=False)
cli_parser.add_argument('-t', '--text-input', action="store_true", dest="text", default=False)

# Parse CLI args and pass to cobra if script runs in CLI
cli_args = cli_parser.parse_args()
cli_cobra = Cobra()

inputType = IO.cli if cli_args.text else IO.file
outputType = IO.cli if not cli_args.output else IO.file
cli_cobra.parse(cli_args.input, cli_args.output, outputType=outputType, inputType=inputType)