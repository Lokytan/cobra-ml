# Cobra Markdown Syntax
Cobra Markdown is a simple enhancement on plain markdown languages. It introduces a variable system which can be
really useful in terms of on-demand changing documents.

## Variables
Variables are placeholder for dynamic contents.

### Names
Variable names **must** begin with an upper/lower-case letter.
The first letter may be followed by these characters:

* Letters (`a-zA-Z`)
* Numbers (`0-9`)
* Special Chars (`-_`)

### Values
Variable values **must** be encapsulated with `"`.
The value can contain any character except the start-token (`@`).

### Declaration

#### Token Structure
| Token | Regex | Example | Description |
|-------|-------|---------|-------------|
| Start | `@`   | <font color=green>@</font>var="value"; | Marker for beginning of a declaration |
| Name  | `([a-zA-Z]+[a-zA-Z0-9_-]+)` | @<font color=green>var</font>="value"; | Name of variable |
| Allocator | `=` | @var<font color=green>=</font>"value" | An allocator which seperates the variable name from value |
| Limiter | `"` | @var=<font color=green>"</font>value<font color=green>"</font> | Limiters which are pre/post seeded to the value |
| Value | `([^@]+)` | @var="<font color=green>value</font>" | Value itself which is stored by variable |
| End | `;` | @var="value"<font color=green>;</font>

#### Example
```cobra-md
@var="value";
```

### Usage
Variable usages are replaced by variables value.
Variable usages are declared by the start-token (`@`), an variable name and the end-token(`;`).

#### Token Structure
| Token | Regex | Example | Description |
|-------|-------|---------|-------------|
| Start | `@` | <font color=green>@</font>var; | Marks the beginning of usage |
| Name | `([a-zA-Z]+[a-zA-Z0-9_-]+)` | @<font color=green>var</font>; | Name of variable |
| End | `;` | @var<font color=green>;</font> | Marks the end of usage |

#### Example
```cobra-md
@var;
```

## Before/After

### Before
```cobra-md
@myName="Mario";
@myAge="20";
@myCountry="germany"

# Hello it's Me

Hello im @myName; and are @myAge; years old. Im living in @myCountry;.
```

### After
```md
# Hello it's me

Hello im Mario and are 20 years old. Im living in germany.
```